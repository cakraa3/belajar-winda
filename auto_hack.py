import os
from sklearn.neighbors import KNeighborsClassifier
import pandas as pd
from pso import PSO
import os, shutil


# Membuat class KNNClassification
class KNNClassification:
    data_training = []  # list kosong untuk menyimpan data training
    data_testing = []   # list kosong untuk menyimpan data testing
    data_training_label = [] # list kosong untuk menyimpan label data training
    data_testing_label = []  # list kosong untuk menyimpan label data testing
    data_testing_filename = [] # list kosong untuk menyimpan filename data testing
    feature_name = []       # list kosong untuk menyimpan nama fitur

    # konstruktor untuk kelas KNNClassification yang menerima beberapa argumen. 
    def __init__(self, training_path, testing_path,
                 feature_name = ['contrast', 'homogeneity', 'energy', 'correlation', 'entropy']):
        # Inisialisasi nama fitur
        self.feature_name = self.feature_name
        # Inisialisai label data training
        csv_training = pd.read_csv(training_path, sep=";")
        self.data_training = csv_training[feature_name].values.tolist()
        self.data_training_label = csv_training['label'].to_list()
        # Inisialisasi label data testing
        csv_testing = pd.read_csv(testing_path, sep=";")
        self.data_testing = csv_testing[feature_name].values.tolist()
        self.data_testing_label = csv_testing['label'].to_list()
        self.data_testing_filename = csv_testing['file'].to_list()

    # Mendefinisikan method compute_accuracy dengan tiga variabel
    def compute_accuracy(self, Y_pred, Y_true = None):
        # Jika Y_true tidak diberikan, maka gunakan data_testing_label sebagai nilai default
        Y_true = Y_true if Y_true else self.data_testing_label
        # Inisialisasi variabel untuk menyimpan jumlah prediksi yang benar
        correctly_predicted = 0  
        # iterasi setiap label dan memeriksa apakah sesuai dengan contoh asli  
        for true_label, predicted in zip(Y_true, Y_pred):  
            if true_label == predicted:  
                correctly_predicted += 1  
        # komputasi nilai akurasi 
        accuracy_score = correctly_predicted*100 / len(Y_true)  
        return accuracy_score

    # Mendefinisikan fungsi klasifikasi dengan parameter k sebagai jumlah tetangga terdekat yang akan digunakan
    def classification(self, k):
        # Membuat objek model KNN dengan jumlah tetangga sebanyak k
        model = KNeighborsClassifier(n_neighbors=k)
        # Melatih model dengan data training dan labelnya
        model.fit(self.data_training, self.data_training_label)
        # Melakukan prediksi terhadap data testing dengan model yang telah dilatih
        result = model.predict(self.data_testing)
        # Mengembalikan hasil prediksi
        return result

    # Mendefinisikan fungsi untuk menghitung error rate pada klasifikasi dengan k
    def classification_error_rate(self, k):
        # melakukan klasifikasi dengan k
        result = self.classification(k)
        # menghitung error rate dengan memanggil fungsi compute_accuracy
        # dan menguranginya dari 100%
        return 100-self.compute_accuracy(result)

    # Mendefinisikan Fungsi untuk mengubah hasil prediksi menjadi file CSV
    def result_to_csv(self, Y_pred = None, name=None):
        # Mengambil data file testing, label, dan hasil prediks
        data = {
            'file': self.data_testing_filename,
            'label': self.data_testing_label,
            'predicted': Y_pred.tolist(),
        }
        # Mengonversi data ke dalam format dataframe
        df = pd.DataFrame(data)
        # Menambah kolom "status" berisi nilai True jika label dan prediksi sama, False jika berbeda
        df.loc[df['label'] == df['predicted'], 'status'] = 'True'
        df.loc[df['label'] != df['predicted'], 'status'] = 'False'
        # Menyimpan data dalam format CSV dengan nama file "testing_predicted.csv"
        df.to_csv(f'testing_predicted.csv', sep=';', index=False)

akurasi_terbaik = 77

while True:
    # Menjalankan file hack.py untuk melakukan proses hacking
    os.system('python hack.py')
    # Menjalankan file hack_sync.py untuk melakukan proses sinkronisasi data hacking
    os.system('python hack_sync.py')
    # Menjalankan file extraction.py untuk melakukan proses ekstraksi data hasil hacking
    os.system('python extraction.py')

    # Membuat objek model
    model = KNNClassification(
        training_path='data/training/scaling_gclm_data.csv',
        testing_path='data/testing/scaling_gclm_data.csv')

    # Import modul process_time dari library time
    from time import process_time
    """ KNN """
    # Inisialisasi waktu awal proses
    t1_start = process_time()
    # Inisiasi variabel nbest dan accuracybest
    nbest = 0
    accuracybest = 0
    # melakukan loop dari k=1 hingga k=179
    for k in range(1, 300):
        # melakukan klasifikasi menggunakan KNN
        result = model.classification(k)
        # menghitung akurasi menggunakan metode compute_accuracy
        n = model.compute_accuracy(result)
        # jika nilai akurasi terbaik ditemukan, update nbest dan accuracybest
        if n > accuracybest:
            accuracybest = n
            nbest = k
    # menghentikan waktu proses
    t1_stop = process_time()
    # mencetak hasil nilai K terbaik dan akurasinya beserta waktu proses
    print(f'Nilai K terbaik dari KNN adalah {nbest} dengan akurasi {accuracybest} dengan waktu {t1_stop-t1_start} detik')

    akurasi = accuracybest

    """ PSO-KNN """
    # Inisialisasi waktu awal proses
    t2_start = process_time()
    # Membuat objek PSO dengan parameter 
    pso = PSO(n=100, nmax=300, func=model.classification_error_rate, max_iteration = 5)
    # Melakukan optimasi dengan PSO dan mendapatkan nilai K terbaik dan akurasinya
    nbest, accuracybest = pso.optimize()
    # menghentikan waktu proses
    t2_stop = process_time()
    # mencetak hasil nilai K terbaik, akurasi, dan waktu yang digunakan
    print(f'Nilai K terbaik dari PSO-KNN adalah {nbest} dengan akurasi {accuracybest} dengan waktu {t2_stop-t2_start} detik')

    # Melakukan prediksi dengan menggunakan K terbaik yang telah didapatkan dari PSO-KNN
    result = model.classification(nbest)
    # Menyimpan hasil prediksi ke dalam file csv
    model.result_to_csv(result)

    if akurasi >= akurasi_terbaik:
        akurasi_terbaik = akurasi
        shutil.copy('data/training/training.csv',f'data/training/training_{int(akurasi)}.csv')
        shutil.copy('data/testing/testing.csv',f'data/testing/testing_{int(akurasi)}.csv')
