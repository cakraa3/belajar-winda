from pathlib import Path
from skimage.feature import graycomatrix, graycoprops
import cv2
import pandas as pd
from sklearn.metrics.cluster import entropy
from sklearn import preprocessing

# Membuat class bernama ExtractionFeatures
class ExtractionFeatures:
    # Membaca file excel dataset yang berisi data aktual dari folder sample_data
    actual_dataset = pd.read_excel('data/sample_data/RoCoLe-classes.xlsx')
    # Mendefinisikan nama fitur yang akan dihitung
    feature_name = ['contrast', 'homogeneity', 'energy', 'correlation', 'entropy']

    def get_gclm(self, file):
        # Mendapatkan path file
        input_path = str(file)
        # Membaca gambar menggunakan OpenCV
        img = cv2.imread(input_path)
        # Mengubah warna gambar menjadi RGB
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        # Mengubah gambar menjadi grayscale
        img = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
        # Menghitung gray-level co-occurrence matrix (GLCM)
        glcm = graycomatrix(img, distances=[1], angles=[0], levels=256,
                symmetric=True, normed=True)
        # Menghitung kontras gambar menggunakan GLCM
        cs = graycoprops(glcm, 'contrast')[0,0]
        # Menghitung homogenitas gambar menggunakan GLCM
        hom = graycoprops(glcm, 'homogeneity')[0,0]
        # Menghitung energi gambar menggunakan GLCM
        eng = graycoprops(glcm, 'energy')[0,0]
        # Menghitung korelasi gambar menggunakan GLCM
        kor = graycoprops(glcm, 'correlation')[0,0]
        # Menghitung entropy gambar
        epy = entropy(img)
        # Mengembalikan hasil pengolahan gambar dalam bentuk list
        return [cs, hom, eng, kor, epy]
    
    """
    Extract from photo to gclm parameters
    """ 
    # Definisikan fungsi "extract" dengan dua parameter, yaitu "input_path" dan "output_path"
    def extract(self, input_path, output_path, data_training_path=None):
        # GCLM Scaling
        # Inisialisasi list untuk menyimpan data feature dan dictionary untuk menyimpan atribut
        data_features = []
        # Definisikan key 'filename' dan 'label' pada dictionary attributes
        attributes = {}
        attributes['filename'] = []
        attributes['label'] = []
        # Looping melalui setiap file dengan ekstensi .png pada direktori input_path
        for file in Path(input_path).glob('*.png'):
            # Panggil fungsi get_gclm untuk menghitung GCLM scaling dari file yang sedang diproses
            gclm_features = self.get_gclm(file)
            # Ambil nama file tanpa ekstensi .png dan tambahkan ekstensi .jpg pada variable filename
            filename = str(file.stem)+'.jpg'
            # Tambahkan filename dan label pada dictionary attributes
            attributes['filename'].append(filename)
            attributes['label'].append(self.actual_dataset.loc[self.actual_dataset['File']==filename]['Binary.Label'].values[0])
            # Tambahkan hasil ekstraksi fitur pada list data_features
            data_features.append(gclm_features)

        # Buat dataframe dari attributes dan data_features
        df1 = pd.DataFrame(attributes['filename'], columns = ['file']) # dataframe untuk nama file
        df2 = pd.DataFrame(data_features, columns = self.feature_name) # dataframe untuk fitur
        df3 = pd.DataFrame(attributes['label'], columns = ['label'])    # dataframe untuk label kelas
        df = pd.concat([df1,df2,df3], axis=1) # gabungkan ketiga dataframe menjadi satu dataframe
        # Export dataframe ke file csv
        df.to_csv(f'{output_path}/gclm_data.csv', sep=';', index=False)

        # MinMax Scaling
        # data_extract diisi dengan nilai df yang merupakan DataFrame hasil prediksi
        data_extract = df
        if data_training_path: # jika ada path data training, maka dilakukan penggabungan antara data prediksi dan data training
            df_training = pd.read_csv(f'{data_training_path}/gclm_data.csv', sep=';')
            data_extract = pd.concat([df, df_training])

        attributes = [] # inisialisasi list attributes
        for i in self.feature_name: # iterasi setiap feature yang digunakan
            attributes.append(data_extract[i].to_list()) # tambahkan nilai feature ke dalam list attributes

        # Melakukan scaling menggunakan MinMaxScaler
        sc = preprocessing.MinMaxScaler()
        data_features = sc.fit_transform(list(zip(*attributes)))
        # Membuat dataframe baru dari data yang sudah discaling
        df2 = pd.DataFrame(data_features, columns = self.feature_name).iloc[:df.index.stop]
        df = pd.concat([df1,df2,df3], axis=1)
        # Menyimpan data yang sudah discaling ke dalam file CSV
        df.to_csv(f'{output_path}/scaling_gclm_data.csv', sep=';', index=False)

# Ekstraksi fitur
obj = ExtractionFeatures()
# Ekstraksi fitur untuk data training 
input_path = 'data/training/normalized'
output_path = 'data/training'
obj.extract(input_path, output_path)
# Ekstraksi fitur untuk data testing 
input_path = 'data/testing/normalized'
output_path = 'data/testing'
data_training_path = 'data/training'
obj.extract(input_path, output_path, data_training_path)
