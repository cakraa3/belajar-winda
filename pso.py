from random import randint
import heapq

# Membuat class bernama PSO
class PSO:
    # Inisialisasi parameter PSO
    n=0
    nmax=0
    w=0.7
    c1=2
    c2=2
    r1=0.5
    r2=0.5
    func=None
    max_iteration=4

    """ 
    Keterangan 
    n = jumlah partikel
    nmax = nilai partikel maximal
    func = fungsi fitness
    """ 
    def __init__(self, n, nmax, func, w=None, c1=None, c2=None, r1=None, r2=None, max_iteration=None):
        # Inisialisasi jumlah partikel
        self.n = n
        # Inisialisasi maksimum iterasi
        self.nmax = nmax
        # Inisialisasi fungsi tujuan
        self.func = func
        # Inisialisasi faktor kecepatan
        self.w = w if w else self.w
        # Inisialisasi konstanta percepatan c1
        self.c1 = c1 if c1 else self.c1
        # Inisialisasi konstanta percepatan c2
        self.c2 = c2 if c2 else self.c2
        # Inisialisasi faktor acak untuk r1
        self.r1 = r1 if r1 else self.r1
        # Inisialisasi faktor acak untuk r2
        self.r2 = r2 if r2 else self.r2
        # Inisiasi iterasi maksimal pada pso
        self.max_iteration = max_iteration if max_iteration else self.max_iteration

    def init_particles(self):
        # Inisialisasi list kosong untuk menampung hasil
        result = []
         # Menambahkan 1 ke dalam list result
        result.append(1)
        # Menambahkan nilai nmax ke dalam list result
        result.append(self.nmax)
        # Melakukan perulangan sebanyak n-2 kali untuk mengisi list result
        for i in range(self.n-2):
            # Mengambil nilai random antara 2 dan nmax-1
            value = randint(2, self.nmax-1)
            # Melakukan perulangan untuk memastikan bahwa nilai random tidak ada di dalam list result
            while value in result:
                value = randint(2, self.nmax-1)
             # Menambahkan nilai yang sudah dicek ke dalam list result
            result.append(value)
        # Mengembalikan list result yang sudah diisi nilai sesuai dengan perulangan
        return result

    def optimize(self):
        pbest = [None for i in range(0, self.n)]     # Inisialisasi variabel pbest sebagai list dengan panjang n, setiap elemennya diisi dengan None
        gbest = None                                 # Inisialisasi variabel gbest dengan nilai None
        velocity = [0 for i in range(0, self.n)]     # Inisialisasi variabel velocity sebagai list dengan panjang n, setiap elemennya diisi dengan angka 0
        fitness = [100 for i in range(0, self.n)]    # Inisialisasi variabel fitness sebagai list dengan panjang n, setiap elemennya diisi dengan angka 100
        particles = self.init_particles()            # Memanggil fungsi init_particles() untuk menghasilkan list partikel awal
        fitness_temp = {}                            # Inisialisasi variabel fitness_temp sebagai dictionary kosong
        max_iteration = self.max_iteration           # Inisialisasi variabel max_iteration dengan nilai self.max_iteration

        # Melakukan perulangan while dengan kondisi sebagai berikut:
        # Jika gbest belum diinisialisasi (gbest == None) atau semua partikel belum sama dan max_iteration masih lebih dari 0, 
        # maka perulangan dilanjutkan
        while gbest == None or not all(x == particles[0] for x in particles) and max_iteration > 0:
            # Melakukan perulangan untuk setiap partikel pada list particles
            for i, k in enumerate(particles):
                # Mencari nilai fitness (error rate)
                # Jika nilai fitness untuk partikel k sudah pernah dicari sebelumnya (k ada di dalam dictionary fitness_temp), 
                # maka nilai fitness diambil dari dictionary
                # Jika belum pernah dicari sebelumnya, maka fungsi self.func() dipanggil untuk menghitung nilai fitness
                if k in fitness_temp:
                    _fitness = fitness_temp[k]
                else:

                    _fitness = self.func(k)
                    fitness_temp[k] = _fitness
                # Mencari nilai pbest
                # Jika nilai fitness partikel k lebih baik dari nilai fitness pbest ke-i, 
                # maka nilai fitness pbest ke-i diupdate dengan nilai fitness k, dan nilai k dijadikan pbest ke-i
                if _fitness < fitness[i]:
                    fitness[i] = _fitness
                    pbest[i] = k
                    

            # Mencari nilai gbest
            if gbest == None or fitness_temp[gbest] > min(fitness):
                gbest = pbest[fitness.index(min(fitness))]

            # Mencari posisi partikel baru
            for i, k in enumerate(particles):
                # Mencari nilai velocity/kecepatan partikel yang baru
                _velocity = self.w * velocity[i] + (self.c1 * self.r1 * (pbest[i] - k)) + (self.c2 * self.r2 * (gbest - k))
                velocity[i] = int(_velocity + 0.5)
                # Posisi nilai partikel yang baru
                new_particle = particles[i] + velocity[i]
                particles[i] = new_particle if new_particle > 0 else particles[i]

            max_iteration-=1

        accuracy = 100-min(fitness)
        return (gbest, accuracy)
