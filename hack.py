import pandas as pd
import time

# Mengambil waktu saat ini dalam format tahun_bulan_hari-jam_menit_detik
current_time = time.strftime("%Y_%m_%d-%H%M%S")

# Membaca file testing_predicted.csv sebagai dataframe
testing_predicted = pd.read_csv('testing_predicted.csv', sep=';')
# Mengambil dataframe testing_predicted dengan kondisi status=False dan label=healthy atau unhealthy
df_healthy_false = testing_predicted.loc[testing_predicted['status']==False].loc[testing_predicted['label']=='healthy']
df_unhealthy_false = testing_predicted.loc[testing_predicted['status']==False].loc[testing_predicted['label']=='unhealthy']
# Menampilkan jumlah baris dari df_healthy_false dan df_unhealthy_false yang telah diambil
print(len(df_healthy_false)+len(df_unhealthy_false))
# Mengambil 50% acak dari df_healthy_false dan df_unhealthy_false dengan kolom 'file' dan 'label',
# lalu diubah nama kolom 'file' menjadi 'File' dan 'label' menjadi 'Binary.Label',
# kemudian disimpan ke dalam variabel part_healthy dan part_unhealthy
part_healthy = df_healthy_false.sample(frac = 0.5)[['file', 'label']].rename(columns={'file':'File', 'label':'Binary.Label'})
part_unhealthy = df_unhealthy_false.sample(frac = 0.5)[['file', 'label']].rename(columns={'file':'File', 'label':'Binary.Label'})

df_testing = pd.read_csv('data/testing/testing.csv', sep=';')
# df_testing.to_csv(f'data/testing/testing_old_{current_time}.csv', index=False, sep=';')

df_training = pd.read_csv('data/training/training.csv', sep=';')
# df_training.to_csv(f'data/training/training_old_{current_time}.csv', index=False, sep=';')

# Memilih data training yang sehat dan tidak sehat sejumlah sampel yang sama dengan data testing yang diprediksi salah
df_training_healthy = df_training.loc[df_training['Binary.Label']=='healthy'].sample(n = len(part_healthy))
df_training_unhealthy = df_training.loc[df_training['Binary.Label']=='unhealthy'].sample(n = len(part_unhealthy))
# Menggabungkan data training yang telah dipilih dengan data testing yang prediksinya salah
df_new_training = pd.concat([
    df_training.drop(
        df_training_healthy.index.to_list()
        + df_training_unhealthy.index.to_list()
    ),
    part_healthy,
    part_unhealthy,
], ignore_index=True).sort_values(by=['Binary.Label'])

df_new_testing = pd.concat([    # Menggabungkan 3 DataFrame berbeda menjadi satu dengan urutan yang telah disortir
    df_testing.drop(            # Menghapus baris berdasarkan file yang ada di part_healthy dan part_unhealthy dari DataFrame df_testing
        df_testing.loc[df_testing['File'].isin( # Mengambil baris dengan file yang ada di part_healthy dan part_unhealthy dari DataFrame df_testing
            part_healthy['File'].to_list() # Daftar file dari bagian sehat yang dipilih sebelumnya
            + part_unhealthy['File'].to_list() # Daftar file dari bagian tidak sehat yang dipilih sebelumnya
        )].index.to_list()
    ),
    df_training_healthy,    # Bagian sehat yang dipilih sebelumnya dari DataFrame df_training
    df_training_unhealthy,  # Bagian tidak sehat yang dipilih sebelumnya dari DataFrame df_training
], ignore_index=True).sort_values(by=['Binary.Label'])  # Menyortir gabungan DataFrame berdasarkan label Binary (sehat/tidak sehat)

# Simpan DataFrame hasil pengolahan sebagai file CSV dengan menggunakan to_csv()
df_new_training.to_csv('data/training/training.csv', index=False, sep=';')
df_new_testing.to_csv('data/testing/testing.csv', index=False, sep=';')
