import pandas as pd
import os

# Membaca file excel RoCoLe-classes.xlsx dan disimpan di variabel dataset
dataset = pd.read_excel('data/sample_data/RoCoLe-classes.xlsx')
# Inisialisasi variabel labels untuk menyimpan hasil pengelompokkan  
labels = dataset[['Binary.Label']].groupby(['Binary.Label']).mean()
# Inisialisasi dataframe untuk data training dan testing
df_training = pd.DataFrame()
df_testing = pd.DataFrame()

# Perulangan untuk setiap label
for i in labels.index:
    # Mengambil data Binar.Label berdasarkan nilai i
    df = dataset.loc[dataset['Binary.Label']==i]
    # Membagi data menjadi data training dan data testing dengan perbandingan 60:40
    part_training = df.sample(frac = 0.6)[['File', 'Binary.Label']]
    part_testing = df.drop(part_training.index)[['File', 'Binary.Label']]
    # Menggabungkan data training dan testing untuk setiap label menjadi satu dataframe
    df_training = pd.concat([df_training, part_training])
    df_testing = pd.concat([df_testing, part_testing])

# Membuat Folder "jika folder belum ada"
if not os.path.isdir('data/training'):
    os.makedirs('data/training')
    os.makedirs('data/training/normalized')
if not os.path.isdir('data/testing'):
    os.makedirs('data/testing')
    os.makedirs('data/testing/normalized')

# Menyimpan data training dan data testing ke file csv
df_training.to_csv('data/training/training.csv', index=False, sep=';')
df_testing.to_csv('data/testing/testing.csv', index=False, sep=';')