import cv2
from rembg import remove, new_session, naive_cutout
import numpy as np
from PIL import Image
import threading
from pathlib import Path
import pandas as pd
import os, shutil


# Membuat class bernama Preprocessing
class Preprocessing:
    def automatic_brightness_and_contrast(self, image, clip_hist_percent=7):
        # Konversi gambar ke grayscale
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

        # Menghitung histogram grayscale
        hist = cv2.calcHist([gray],[0],None,[256],[0,256])
        hist_size = len(hist)

        # Menghitung distribusi kumulatif dari histogram
        accumulator = []
        accumulator.append(float(hist[0]))
        for index in range(1, hist_size):
            accumulator.append(accumulator[index -1] + float(hist[index]))

        maximum = accumulator[-1]
        clip_hist_percent *= (maximum/100.0)
        clip_hist_percent /= 2.0

        # Menentukan nilai minimum dan maksimum grayscale yang akan digunakan
        minimum_gray = 0
        while accumulator[minimum_gray] < clip_hist_percent:
            minimum_gray += 1

        maximum_gray = hist_size -1
        while accumulator[maximum_gray] >= (maximum - clip_hist_percent):
            maximum_gray -= 1

        # Menghitung alpha dan beta
        alpha = 255 / (maximum_gray - minimum_gray)
        beta = -minimum_gray * alpha

        # Mengaplikasikan penyesuaian brightness dan contrast pada gambar
        auto_result = cv2.convertScaleAbs(image, alpha=alpha, beta=beta)
        return auto_result

    # Fungsi untuk menghilangkan background pada gambar menggunakan U-2 NET
    def remove_background(self, image, session_type="u2net"):
        # Men-skala citra
        scaleed_image = self.automatic_brightness_and_contrast(image)
        # Mengubah gambar menjadi grayscale
        image_gray = cv2.cvtColor(scaleed_image, cv2.COLOR_BGR2GRAY)
        # Menghilangkan background menggunakan U-2 NET
        mask = remove(image_gray, session=new_session(session_type), alpha_matting=True, only_mask=True, post_process_mask=True)
        # Mengubah gambar dan mask menjadi objek Image
        image = Image.fromarray(image)
        mask = Image.fromarray(mask)
        # Membuat gambar hasil cutout dari gambar dan mask
        cutout = naive_cutout(image, mask)
        # Mengubah hasil cutout menjadi array numpy dan mengembalikannya
        return np.asarray(cutout)

    # Fungsi untuk memotong gambar agar hanya tersisa objek dalam gambar
    def crop_image(self, image):
        # Mengambil koordinat piksel yang tidak kosong (nilainya bukan 0)
        y,x = image[:,:,3].nonzero()
        # Menentukan nilai x dan y maksimum dan minimum
        minx = np.min(x)
        miny = np.min(y)
        maxx = np.max(x)
        maxy = np.max(y)
        # Memotong gambar berdasarkan nilai x dan y maksimum dan minimum
        cropImg = image[miny:maxy, minx:maxx]
        return cropImg

      # Fungsi untuk mengubah ukuran gambar dengan parameter input adalah gambar, lebar, tinggi, dan jenis interpolasi 

    def resize_image(self, image, width = None, height = None, inter = cv2.INTER_AREA):
        # Inisialisasi dimensi menjadi none
        dim = None
        # Mendapatkan ukuran gambar dari shape
        (h, w) = image.shape[:2]
        #Jika tidak ada lebar dan tinggi yang diberikan, kembalikan gambar
        if width is None and height is None:
            return image
        # Jika ada salah satu dimensi yang diberikan, hitung dimensi lainnya dengan menjaga rasio aspek
        if None not in [width, height]:
            dim = (width, height)
        elif width is None:
            r = height / float(h)
            dim = (int(w * r), height)
        else:
            r = width / float(w)
            dim = (width, int(h * r))
        # Mengubah ukuran gambar dengan dimensi baru dan jenis interpolasi yang diberikan
        resized = cv2.resize(image, dim, interpolation = inter)
        return resized

    # Mendefinisikan method process yang menerima input file dan output_path
    def process(self, file, output_path):
        try:
            # Mendapatkan path file dan nama file
            input_path = str(file)
            filename = f"{file.stem}.png"
            # Membaca input gambar, menghapus background, memotong gambar, dan mengubah ukuran gambar
            input = cv2.imread(input_path)
            background_removed = self.remove_background(input)
            croped_image = self.crop_image(background_removed)
            resized_image = self.resize_image(croped_image, width=200, height=200)
             # Menyimpan gambar hasil olahan ke dalam folder output_path dengan nama file yang telah ditentukan
            cv2.imwrite(f"{output_path}/{filename}", resized_image)
            print(f"{file.name} success")
        except Exception as e:
            # Menampilkan pesan error jika terjadi kesalahan saat memproses gambar
            print(f'{file.name} error!')


# Membaca data testing dan training dari file csv
df_testing = pd.read_csv('data/testing/testing.csv', sep=';')
df_training = pd.read_csv('data/training/training.csv', sep=';')

# Membuat objek Preprocessing
preprocessing = Preprocessing()

# Looping untuk setiap file pada data testing
for index, filename in enumerate(df_testing['File']):
    # Menentukan path output
    output_path = f'data/testing/normalized'
    try:
        # Mengubah format file jpg menjadi png dan memindahkan dari data training ke data testing
        filename_png = filename.replace('jpg','png')
        if not os.path.isfile(f'{output_path}/{filename_png}'):
            shutil.move(f'data/training/normalized/{filename_png}', f'{output_path}/{filename_png}')
    except Exception as e:
        # Jika file tidak ada di data training, mencari di data all_normalized
        try:
            shutil.move(f'data/all_normalized/{filename_png}', f'{output_path}/{filename_png}')
        except Exception as e:
            # Jika file belum dinormalisasi, melakukan proses preprocessing dan menyimpan ke data 
            image = Path(f'data/sample_data/images/{filename}')
            preprocessing.process(image, output_path)
            shutil.copy(f'{output_path}/{filename_png}', f'data/all_normalized/{filename_png}')

for filename in df_training['File']:
    output_path = f'data/training/normalized'
    try:
        # Jika file png belum ada di folder output, 
        # maka pindahkan file png dari folder testing/normalized ke folder training/normalized
        filename_png = filename.replace('jpg','png')
        if not os.path.isfile(f'{output_path}/{filename_png}'):
            shutil.move(f'data/testing/normalized/{filename_png}', f'{output_path}/{filename_png}')
    except Exception as e:
        try:
            # Jika file png belum ada di folder output dan tidak tersedia di folder testing/normalized, 
            # maka pindahkan dari folder all_normalized ke folder training/normalized
            shutil.move(f'data/all_normalized/{filename_png}', f'{output_path}/{filename_png}')
        except Exception as e:
            # Jika file png tidak ada di folder output, testing/normalized, dan all_normalized, 
            # maka lakukan preprocessing pada gambar asli dan pindahkan hasil preprocessing ke folder output dan all_normalized
            image = Path(f'data/sample_data/images/{filename}')
            preprocessing.process(image, output_path)
            shutil.copy(f'{output_path}/{filename_png}', f'data/all_normalized/{filename_png}')

# Menghapus gambar yang tidak terdaftar dalam file csv training
print('remove training')
for file in Path('data/training/normalized').glob('*.png'):
    if df_training.loc[df_training['File'] == file.stem + '.jpg'].empty:
        print(file.name)
        os.remove('data/training/normalized/' + file.name)

# Menghapus gambar yang tidak terdaftar dalam file csv testing
print('remove testing')
for file in Path('data/testing/normalized').glob('*.png'):
    if df_testing.loc[df_testing['File'] == file.stem + '.jpg'].empty:
        print(file.name)
        os.remove('data/testing/normalized/' + file.name)

# Menampilkan pesan "hack sync success!"
print('hack sync success!')