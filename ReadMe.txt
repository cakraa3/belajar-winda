Installation:
1. Install Python Version >3.7 and <3.11
2. Install virtualenv
>> pip install virtualenv
>> virtualenv .venv
3. Use the virtualenv
>> Windows = type ".venv/Scripts/activate"
4. Install requirements.txt
>> pip install -r requirements.txt


Urutan:
1. python data_split.py
2. python preprocessing.py
3. python extraction.py
4. python classification.py
