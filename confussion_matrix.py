import pandas as pd
from sklearn.metrics import classification_report

# Membaca file CSV 'testing_predicted.csv' dan memisahkan kolom-kolomnya dengan separator ';'
df = pd.read_csv('testing_predicted.csv', sep=';')
# Membuat dictionary berisi data aktual dan data prediksi dari file CSV 'testing_predicted.csv'
data = {
    'y_actual':    df['label'].to_list(),
    'y_predicted': df['predicted'].to_list()
}

# Membuat DataFrame dari dictionary data
df = pd.DataFrame(data)

# Menghitung confusion matrix menggunakan pandas crosstab
confusion_matrix = pd.crosstab(df['y_actual'], df['y_predicted'], rownames=['Actual'], colnames=['Predicted'])
# Menampilkan confusion matrix ke layar
print(confusion_matrix)
print('\n')
# Menghitung dan menampilkan classification report ke layar
print(classification_report(df['y_actual'], df['y_predicted'], digits=3))