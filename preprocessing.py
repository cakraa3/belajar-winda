import cv2
from rembg import remove, new_session, naive_cutout
import numpy as np
from PIL import Image
import threading
from pathlib import Path
import pandas as pd
import os, shutil

# Membuat class bernama Preprocessing
class Preprocessing:
    def automatic_brightness_and_contrast(self, image, clip_hist_percent=7):
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

        # Menghitung histogram grayscale
        hist = cv2.calcHist([gray],[0],None,[256],[0,256])
        hist_size = len(hist)

        # Menghitung distribusi kumulatif dari histogram
        accumulator = []
        accumulator.append(float(hist[0]))
        for index in range(1, hist_size):
            accumulator.append(accumulator[index -1] + float(hist[index]))

        maximum = accumulator[-1]
        clip_hist_percent *= (maximum/100.0)
        clip_hist_percent /= 2.0

        minimum_gray = 0
        while accumulator[minimum_gray] < clip_hist_percent:
            minimum_gray += 1

        maximum_gray = hist_size -1
        while accumulator[maximum_gray] >= (maximum - clip_hist_percent):
            maximum_gray -= 1

        # Menghitung alpha dan beta
        alpha = 255 / (maximum_gray - minimum_gray)
        beta = -minimum_gray * alpha

        auto_result = cv2.convertScaleAbs(image, alpha=alpha, beta=beta)
        return auto_result

    # Fungsi untuk menghilangkan background pada gambar menggunakan U-2 NET
    def remove_background(self, image, session_type="u2net"):
        # Men-skala citra
        scaleed_image = self.automatic_brightness_and_contrast(image)
        # Mengubah gambar menjadi grayscale
        image_gray = cv2.cvtColor(scaleed_image, cv2.COLOR_BGR2GRAY)
        # Menghilangkan background menggunakan U-2 NET
        mask = remove(image_gray, session=new_session(session_type), alpha_matting=True, only_mask=True, post_process_mask=True)
        # Mengubah gambar dan mask menjadi objek Image
        image = Image.fromarray(image)
        mask = Image.fromarray(mask)
        # Membuat gambar hasil cutout dari gambar dan mask
        cutout = naive_cutout(image, mask)
        # Mengubah hasil cutout menjadi array numpy dan mengembalikannya
        return np.asarray(cutout)

    # Fungsi untuk memotong gambar agar hanya tersisa objek dalam gambar
    def crop_image(self, image):
        # Mengambil koordinat piksel yang tidak kosong (nilainya bukan 0)
        y,x = image[:,:,3].nonzero()
        # Menentukan nilai x dan y maksimum dan minimum
        minx = np.min(x)
        miny = np.min(y)
        maxx = np.max(x)
        maxy = np.max(y)
        # Memotong gambar berdasarkan nilai x dan y maksimum dan minimum
        cropImg = image[miny:maxy, minx:maxx]
        return cropImg

      # Fungsi untuk mengubah ukuran gambar dengan parameter input adalah gambar, lebar, tinggi, dan jenis interpolasi 

    def resize_image(self, image, width = None, height = None, inter = cv2.INTER_AREA):
        # Inisialisasi dimensi menjadi none
        dim = None
        # Mendapatkan ukuran gambar dari shape
        (h, w) = image.shape[:2]
        #Jika tidak ada lebar dan tinggi yang diberikan, kembalikan gambar
        if width is None and height is None:
            return image
        # Jika ada salah satu dimensi yang diberikan, hitung dimensi lainnya dengan menjaga rasio aspek
        if None not in [width, height]:
            dim = (width, height)
        elif width is None:
            r = height / float(h)
            dim = (int(w * r), height)
        else:
            r = width / float(w)
            dim = (width, int(h * r))
        # Mengubah ukuran gambar dengan dimensi baru dan jenis interpolasi yang diberikan
        resized = cv2.resize(image, dim, interpolation = inter)
        return resized

    # Mendefinisikan method process yang menerima input file dan output_path
    def process(self, file, output_path):
        try:
            # Mendapatkan path file dan nama file
            input_path = str(file)
            filename = f"{file.stem}.png"
            # Membaca input gambar, menghapus background, memotong gambar, dan mengubah ukuran gambar
            input = cv2.imread(input_path)
            background_removed = self.remove_background(input)
            croped_image = self.crop_image(background_removed)
            resized_image = self.resize_image(croped_image, width=200, height=200)
            cv2.imwrite(f"{output_path}/{filename}", resized_image)

            # Mengulang proses jika gambar tidak baik dengan menggunakan u2net_human_seg
            img = cv2.imread(f"{output_path}/{filename}", 0)
            count = cv2.countNonZero(img)
            if count < 20000 or count > 34000:
                background_removed = self.remove_background(input, session_type='u2net_human_seg')
                croped_image = self.crop_image(background_removed)
                resized_image = self.resize_image(croped_image, width=200, height=200)
                cv2.imwrite(f"{output_path}/{filename}", resized_image)
            print(f"{file.name} success")
        except Exception as e:
            # Menampilkan pesan error jika terjadi kesalahan saat memproses gambar
            print(f'{file.name} error!')

# Menghapus folder data yang sudah dinormalisasi jika sudah ada
if os.path.isdir('data/training/normalized'):
    shutil.rmtree('data/training/normalized')
os.makedirs('data/training/normalized')
if os.path.isdir('data/testing/normalized'):
    shutil.rmtree('data/testing/normalized')
os.makedirs('data/testing/normalized')

# Menormalisasikan dataset
# Menginisialisasi path folder gambar input
input_path = 'data/sample_data/images'
# Membuat objek preprocessing
preprocessing = Preprocessing()
# Membaca data training dan testing dari file csv
data_training = pd.read_csv('data/training/training.csv', sep=';')
data_testing = pd.read_csv('data/testing/testing.csv', sep=';')
# Membuat sebuah list kosong threads
threads = []
# Melakukan iterasi pada setiap file gambar dengan format jpg yang terdapat di dalam folder input_path
for file in Path(input_path).glob('*.jpg'):
    if len(threads) >= 10:
        list(map(lambda x: x.join(), threads))
        threads = []
    # Menentukan output_path berdasarkan apakah file gambar tersebut terdapat di dalam data training atau data testing
    output_path = (
        'data/training/normalized'
        if len(data_training.loc[data_training['File']==file.name].index)
        else 'data/testing/normalized'
    )
    # Membuat sebuah thread yang menjalankan fungsi process dari objek preprocessing, dengan parameter file dan output_path
    t = threading.Thread(target=preprocessing.process, args=(file, output_path))
    # Memulai thread
    t.start()
    # Menambahkan thread ke dalam list threads
    threads.append(t)